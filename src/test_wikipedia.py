import unittest

from src.wikipedia import Wikipedia


class WikipediaCase(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.obj = Wikipedia()

    def test_encode_page_title(self):
        tests = [
            ("Bitches Ain't Shit", "Bitches_Ain%27t_Shit"),
        ]
        for page_title, expected in tests:
            self.assertEqual(expected, self.obj.encode_page_title(page_title))


if __name__ == '__main__':
    unittest.main()
