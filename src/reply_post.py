import os
from json import loads, dumps
from typing import List, Optional, Tuple
from urllib.parse import urlparse

import praw
import stanza
from praw.reddit import Submission
from stanza import Pipeline

from wikipedia import Wikipedia


def log_into_reddit():
    reddit = praw.Reddit('bot1')
    print(reddit.user.me())
    return reddit

# variable
ANALYTICS_JSON = "posts_analytics.json"


def get_posts_analytics() -> dict:
    if not os.path.isfile(ANALYTICS_JSON):
        posts_analytics = {}

    # If we have run the code before, load the list of posts we have replied to
    else:
        # Read the file into a list and remove any empty values
        with open(ANALYTICS_JSON, "r") as f:
            posts_analytics = loads(f.read())
    return posts_analytics

def initiate_nlp() -> Pipeline:
    stanza.download('en')
    nlp_pipe = stanza.Pipeline('en', processors="tokenize,pos")
    return nlp_pipe

def fetch_reddit_posts(selected_subreddit: str, limit: int) -> List[Submission]:
    subreddit = reddit.subreddit(selected_subreddit)
    return subreddit.new(limit=limit)

def process_post(post, nlp_pipe: Pipeline) -> Tuple[int, str, List[str], Optional[str], int]:
    doc = nlp_pipe(post.title)
    keywords = get_keywords_from_post(doc.sentences)
    # write all keywords in lower case
    keywords = [keyword.lower() for keyword in keywords]
    # remove all duplicates and keep original order
    unique_keywords = list(dict.fromkeys(keywords))
    print(" ".join(unique_keywords))
    print(f"{post.title}")
    print(f"https://www.reddit.com/r/HistoryPorn/comments/{post.id}")
    return (post.id, post.title, unique_keywords) + fetch_wikipedia_data(unique_keywords)


def fetch_wikipedia_data(unique_keywords: List[str]) ->Tuple[Optional[str], int]:
    fetch_wikipedia = Wikipedia()
    return fetch_wikipedia.search(unique_keywords)


def filter_analytics(posts: List[Submission], posts_analytics: dict):
    filtered_posts = {}
    for post in posts:
        key = post.id
        if post.id in posts_analytics:
            continue
        filtered_posts[key] = post
    return filtered_posts


def get_keywords_from_post(sentences: list):
    keywords = []
    for sentence in sentences:
        for word in sentence.words:
            if word.upos not in ['NOUN', 'VERB', 'NUM', 'PROPN']:
                continue
            keywords.append(word.text)
    return keywords

def reply_to_post(post, reply, page_id):
    # Reply to the post
    if reply is None:
        return
    post.reply(f"Did you know that: {reply} Source: https://en.wikipedia.org/wiki/?curid={page_id}")
    print(f"Bot replying to: {post.title} https://www.reddit.com/r/HistoryPorn/comments/{post.id}")

def store_line(f, line):
    f.write(line + "\n")


def get_query_params(url: str) -> dict:
    obj = urlparse(url)
    querystrings = obj.query.split("&")
    params = {}
    for querystring in querystrings:
        key, value = querystring.split("=")
        params[key] = value
    return params

if __name__ == '__main__':
    # log into reddit
    reddit = log_into_reddit()
    # check posts replied to
    # initiate nlp
    nlp_pipe = initiate_nlp()
    # create posts_analytics
    posts_analytics = get_posts_analytics()
    # fetch reddit posts
    posts = fetch_reddit_posts("HistoryPorn", 10)
    analytics_filtered = filter_analytics(posts, posts_analytics)
    # read submission titles
    for post in analytics_filtered.values():
        nlp_data = process_post(post, nlp_pipe)
        _id, _title, unique_keywords, reply, page_id = nlp_data
        posts_analytics[post.id] = nlp_data
        #reply_to_post(post, reply, page_id)
    # store nlp doc in posts_analytics
    with open(ANALYTICS_JSON, "w") as f:
        f.write(dumps(posts_analytics))
