from json import loads
from typing import List, Optional, Tuple
from urllib.parse import quote_plus

import requests

from utilities import cleanhtml


class Wikipedia:
    api_endpoint = "https://en.wikipedia.org/w/api.php"
    default_params = {
        'action': 'query',
        'list': 'search',
        'srsearch': '',
        'format': 'json'
    }

    def search(self, unique_keywords: List[str]) -> Tuple[Optional[str], int]:
        self.default_params['srsearch'] = quote_plus(" ".join(unique_keywords))
        query_params = []
        for key, value in self.default_params.items():
            query_params.append(f"{key}={value}")
        api_url = f"{self.api_endpoint}?{'&'.join(query_params)}"
        response = requests.get(api_url)
        data = loads(response.content)
        results = data.get('query', {}).get('search')
        if not results:
            return None, 0
        page_id = results[0].get('pageid')
        return self.get_page_extract(page_id), page_id

    def get_page_extract(self, page_id: int, num_sentences: int = 3) -> Optional[str]:
        api_url = f"{self.api_endpoint}?action=query&prop=extracts&exsentences={num_sentences}&pageids={page_id}&format=json"
        response = requests.get(api_url)
        if response.status_code == 404:
            print(f'Error for {api_url}: {response.status_code} - {response.reason}')
            return None
        data = loads(response.content)
        result = data.get('query', {}).get('pages', {}).get(str(page_id))
        if not result:
            return None
        clean_result = cleanhtml(result.get('extract'))
        return clean_result.strip()


    def encode_page_title(self, page_title: str):
        return quote_plus(page_title).replace('+', '_')
