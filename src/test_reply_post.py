import unittest

from src.reply_post import get_query_params


class ReplyPostTest(unittest.TestCase):
    def test_something(self):
        self.assertEqual(True, True)  # add assertion here

    def test_url_query(self):
        tests = [
            ('wikipedia', ' https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=steve+-o+graduates+clown+school+1997&format=json',
             {
                 'action': 'query',
                 'list': 'search',
                 'srsearch': 'steve+-o+graduates+clown+school+1997',
                 'format': 'json'
             }),
            ('foobar', 'http://google.com/?foo=bar', {'foo': 'bar'})
        ]

        for comment, url, expected in tests:
            self.assertEqual(expected, get_query_params(url))

if __name__ == '__main__':
    unittest.main()
