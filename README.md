Crappyfactgenerator
=========

A simple Reddit bot in Python.

A software bot is a program that can interact with websites autonomously.
This bot analyzes the title of a Reddit post and creates a 'crappy fact' from a corresponding Wikipedia page by using the [Stanza library](https://stanfordnlp.github.io/stanza) and replies by commenting the previously created fact.

original source:
http://pythonforengineers.com/build-a-reddit-bot-part-1/

